package com.interestech.ch07_ajifauzipangestu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.interestech.ch07_ajifauzipangestu.databinding.ActivitySplashScreenBinding
import com.interestech.ch07_ajifauzipangestu.helper.viewModelsFactory
import com.interestech.ch07_ajifauzipangestu.repo.DatastoreManager
import com.interestech.ch07_ajifauzipangestu.viewmodel.DatastoreViewModel

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private lateinit var handler: Handler
    private val pref: DatastoreManager by lazy { DatastoreManager(this) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        checkLoginState()
    }

    private fun checkLoginState() {
        datastoreViewModel.getLoginState().observe(this) {
            if (it == true) {
                handler = Handler(Looper.getMainLooper())
                handler.postDelayed({
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }, 3000)
            } else {
                handler = Handler(Looper.getMainLooper())
                handler.postDelayed({
                    startActivity(Intent(this, AuthActivity::class.java))
                    finish()
                }, 3000) // 3 detik
            }
        }
    }
}