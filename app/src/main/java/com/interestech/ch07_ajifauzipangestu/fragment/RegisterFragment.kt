package com.interestech.ch07_ajifauzipangestu.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.interestech.ch07_ajifauzipangestu.R
import com.interestech.ch07_ajifauzipangestu.database.User
import com.interestech.ch07_ajifauzipangestu.databinding.FragmentRegisterBinding
import com.interestech.ch07_ajifauzipangestu.helper.viewModelsFactory
import com.interestech.ch07_ajifauzipangestu.repo.UserRepo
import com.interestech.ch07_ajifauzipangestu.viewmodel.RegisterViewModel


class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val registerViewModel: RegisterViewModel by viewModelsFactory { RegisterViewModel(userRepo) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doRegister()
        observeData()
    }

    private fun doRegister() {
        binding.btnDaftar.setOnClickListener {
            // Get value dari TextEditText
            val etUsername = binding.editUsername.editText?.text.toString()
            val etEmail = binding.editEmail.editText?.text.toString()
            val etPassword1 = binding.editPassword1.editText?.text.toString()
            val etPassword2 = binding.editPassword2.editText?.text.toString()

            // Validasi inputan jika tidak ada yg kosong / jml karakter terpenuhi
            if (registerValidation(etUsername, etEmail, etPassword1, etPassword2)) {
                val user = User(null, etUsername, etEmail, etPassword1)
                // Jalankan fungsi addUserToDb di RegisterViewModel
                registerViewModel.addUserToDb(etUsername, etEmail, user)
            }
        }
    }

    private fun registerValidation(username: String, email: String, pass1: String, pass2: String): Boolean {
        var result = true
        if (username.isEmpty()) { // jika kosong
            binding.editUsername.error = "Username tidak boleh kosong!"
            result = false
        } else if (username.length < 5) { // jika kurang dari 5 karakter
            binding.editUsername.error = "Minimum 5 karakter!"
            result = false
        } else {
            binding.editUsername.isErrorEnabled = false
        }

        if (email.isEmpty()) { // jika kosong
            binding.editEmail.error = "Email tidak boleh kosong!"
            result = false
        } else {
            binding.editEmail.isErrorEnabled = false
        }

        if (pass1.isEmpty()) { // jika kosong
            binding.editPassword1.error = "Password tidak boleh kosong!"
            result = false
        } else if (pass1.length < 6) { // jika kurang dari 6 karakter
            binding.editPassword1.error = "Password minimum 6 Karakter!"
            result = false
        } else {
            binding.editPassword1.isErrorEnabled = false
        }

        if (pass2.isEmpty()) { // jika kosong
            binding.editPassword2.error = "Konfirmasi Password tidak boleh kosong!"
            result = false
        } else if (pass2 != pass1) { // jika konfirm pass tdk sama dgn pass
            binding.editPassword2.error = "Password harus sama!"
            result = false
        } else {
            binding.editPassword2.isErrorEnabled = false
        }

        return result
    }

    private fun observeData() {
        registerViewModel.usernameHasRegistered.observe(viewLifecycleOwner) {
            binding.editUsername.error = "Username sudah dipakai"
        }

        registerViewModel.emailHasRegistered.observe(viewLifecycleOwner) {
            binding.editEmail.error = "Email sudah dipakai"
        }

        registerViewModel.statusRegistration.observe(viewLifecycleOwner) {
            if (it) { // jika berhasil
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                Toast.makeText(requireContext(), "Berhasil Daftar", Toast.LENGTH_SHORT).show()
                binding.editUsername.isErrorEnabled = false
                binding.editEmail.isErrorEnabled = false
            } else { // jika gagal
                Toast.makeText(requireContext(), "Gagal Daftar", Toast.LENGTH_SHORT).show()
            }
        }
    }

}