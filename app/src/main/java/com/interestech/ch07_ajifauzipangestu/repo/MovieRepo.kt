package com.interestech.ch07_ajifauzipangestu.repo

import com.interestech.ch07_ajifauzipangestu.service.TMDBApiService

class MovieRepo(private val tmdbApiService: TMDBApiService) {
    suspend fun getMovie(apiKey: String) = tmdbApiService.getAllMovie(apiKey)
    suspend fun getMovieDetail(movieId: Int, apiKey: String) = tmdbApiService.getDetailMovie(movieId, apiKey)
}