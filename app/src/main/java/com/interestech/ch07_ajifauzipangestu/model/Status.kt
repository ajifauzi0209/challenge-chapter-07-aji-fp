package com.interestech.ch07_ajifauzipangestu.model



enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
